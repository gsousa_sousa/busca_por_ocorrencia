from django.test import TestCase
from .models import Sites
from .views import home

class SiteTestCase(TestCase):
    def setUp(self):
        Sites.objects.create(url="https://pt.wikipedia.org/wiki/Marte_(planeta)", palavra="marte")
        Sites.objects.create(url="https://docs.pytest.org/en/latest/", palavra="python")

    def test_sites_ocorrencia(self):
        site_1 = Sites.objects.get(palavra="marte")
        Sites.site_ocorrencia(site_1)


