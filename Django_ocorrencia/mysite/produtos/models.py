from django.db import models

class Sites(models.Model):
    url = models.CharField(max_length=200)
    palavra = models.CharField(max_length=100)
    qtd_ocorrencia = models.IntegerField()

    def __str__(self):
        return self.url


    def site_ocorrencia(self, site):
        return self.qtd_ocorrencia

