import requests
import re
from django.http import JsonResponse

def home(request):
    urls = ''
    qtd_palavra = ''
    palavra = request.GET.get('palavra')
    url = request.GET.get('urls')
    ocorrencias = []
    sites = []
    data = {}


    if url != None:
        urls = url.split(',')
        for u in urls:
            try:
               site = requests.get(u)
            except:
               site = None
            if site != None:
               converter_site = site.text
               converter_site.upper()
               palavra.upper()
               if palavra != None:
                   qtd_palavra = sum(1 for match in re.finditer(r"\b"+palavra.upper()+r"\b", converter_site.upper()))
               ocorrencias.append(qtd_palavra)
               sites.append({"url":u, "ocorencias": qtd_palavra})
    if sites:
        return JsonResponse(sites, safe=False)
    else:
        return JsonResponse({"detail": "informe a url e a palavra a ser buscada"})
